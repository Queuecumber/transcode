import argparse
import os
import subprocess
import sys
import pickle
import io

ffmpeg_command = 'ffmpeg -i "{}" {} {} -map "0:v?" -map "0:a?" -strict -2 -y "{}"'

allowed_video_codecs = ['hevc']
allowed_audio_codecs = ['aac']
allowed_containers = ['.mp4']

video_file_extensions = [
    '.avi',
    '.m4v',
    '.mkv',
    '.mov',
    '.mp4',
    '.mpg',
    '.rm',
    '.webm',
    '.wmv'
]


def extract_codec_name(line, tag):
    if tag in line:
        words = line.split(' ')
        cnamel = words.index(tag) + 1
        return words[cnamel].replace(',', '').strip()

    return None


def probe_codecs(vfile):
    ffprobe_proc = subprocess.Popen(['ffprobe', vfile], stdout=subprocess.PIPE, stderr=subprocess.PIPE, bufsize=1)
    out, err = ffprobe_proc.communicate()
    errlines = err.decode(errors='ignore').splitlines()

    acodecs = []
    vcodecs = []
    for line in errlines:
        line = line.strip()
        if line.startswith('Stream'):
            vcname = extract_codec_name(line, 'Video:')
            acname = extract_codec_name(line, 'Audio:')

            if vcname is not None:
                vcodecs.append(vcname)
            elif acname is not None:
                acodecs.append(acname)

    container = os.path.splitext(vfile)[1]

    return vcodecs, acodecs, container


def codec_commands(prefix, allowed, probed):
    cmap = []
    for i in range(0, len(probed)):
        if probed[i] in allowed:
            cmap.append('-c:{}:{} copy'.format(prefix, i))
        else:
            cmap.append('-c:{}:{} {}'.format(prefix, i, allowed[0]))

    return ' '.join(cmap)


def replace_root(path, new_root):
    dirs = path.split('/')
    dirs[0] = new_root
    return '/'.join(dirs)


def ffmpeg_progress(proc):
    duration = ''
    ctime = ''
    for line in io.TextIOWrapper(proc.stderr, errors='ignore'):
        if 'Duration:' in line:
            parts = line.split(' ')
            i = parts.index(u'Duration:') + 1
            duration = parts[i].replace(',', ' ').strip()
        elif 'time=' in line:
            parts = line.split(' ')
            timestamp = next((i for i in parts if i.startswith('time=')))
            ctime = timestamp.replace('time=', '').replace(',', '').strip()

        if duration != '' and ctime != '':
            sys.stdout.write('\r\t[{}/{}]'.format(ctime, duration))
            sys.stdout.flush()

    sys.stdout.write('\n')


def load_progress(pfile):
    if not os.path.exists(pfile):
        return []

    with open(pfile, 'rb') as f:
        return pickle.load(f)


def write_progress(pfile, p):
    with open(pfile, 'wb') as f:
        pickle.dump(p, f)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input_dir')
    parser.add_argument('output_dir')
    parser.add_argument('--dry-run', dest='dry_run', action='store_true', default=False)
    parser.add_argument('--progress', dest='progress', default=None)
    args = parser.parse_args()

    progress = None
    if args.progress is not None:
        print('Parsing progress from {}'.format(args.progress))
        progress = load_progress(args.progress)

    print("{} => {}".format(args.input_dir, args.output_dir))

    folders = [args.input_dir]

    try:
        while len(folders) > 0:
            cur = folders.pop()

            items = [os.path.join(cur, f) for f in os.listdir(cur)]
            items.sort()

            files = []
            for f in items:
                if os.path.isdir(f):
                    folders.append(f)
                else:
                    files.append(f)

            for f in files:
                if os.path.splitext(f)[1] in video_file_extensions and (progress is None or f not in progress):
                    print('Process {}'.format(f))

                    output_file = replace_root(f, args.output_dir)
                    output_file = os.path.splitext(output_file)[0] + '.mp4'

                    vc, ac, c = probe_codecs(f)
                    print('\tDetected video tracks: {}'.format(vc))
                    print('\tDetected audio tracks: {}'.format(ac))
                    print('\tDetected container: {}'.format(c))

                    needsv = not all(c in allowed_video_codecs for c in vc)
                    needsa = not all(c in allowed_audio_codecs for c in ac)
                    needsc = c not in allowed_containers

                    if needsv or needsa or needsc:
                        vargs = codec_commands('v', allowed_video_codecs, vc)
                        aargs = codec_commands('a', allowed_audio_codecs, ac)

                        convert_command = ffmpeg_command.format(f, vargs, aargs, output_file)
                    else:
                        convert_command = 'cp "{}" "{}"'.format(f, output_file)

                    print('\t{} => {}'.format(f, output_file))
                    print('\t{}'.format(convert_command))

                    if not args.dry_run:
                        os.makedirs(os.path.dirname(output_file), exist_ok=True)
                        ffmpeg_proc = subprocess.Popen(convert_command, shell=True, stdout=subprocess.PIPE,
                                                       stderr=subprocess.PIPE, bufsize=1)
                        if convert_command.startswith('cp'):
                            print('\tCOPY')
                        else:
                            ffmpeg_progress(ffmpeg_proc)

                        ffmpeg_proc.wait()

                        if progress is not None:
                            progress.append(f)
                    else:
                        print('\tDRYRUN MODE')
    except:
        print('Bailing early because of an error')

        if progress is not None:
            write_progress(args.progress, progress)

        raise

    if progress is not None:
        write_progress(args.progress, progress)


if __name__ == '__main__':
    main()
